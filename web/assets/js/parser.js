var Parser = {
    addText: function (text) {
        return "<span class='ace_text'>" + text + "</span>";
    },
    addOpenOption: function () {
        return "<span class='ace_option_start'>(</span>";
    },
    addOption: function (text) {
        return "<span class='ace_option'>" + text + "</span>";
    },
    addCloseOption: function () {
        return "<span class='ace_option_end'>)</span>";
    },
    addOpenCorrectOption: function () {
        return "<span class='ace_correct_option_start'>((</span>";
    },
    addCorrectOption: function (text) {
        return "<span class='ace_correct_option'>" + text + "</span>";
    },
    addCloseCorrectOption: function () {
        return "<span class='ace_correct_option_end'>))</span>"
    },
    format: function (source) {
        var state = "text", result = "", tmp = "";

        for (var i = 0; i < source.length; i++) {
            var char = source[i];

            if (state == 'text') {
                if (char == '(') {
                    result += this.addText(tmp);
                    state = 'open_option';
                    tmp = '';
                } else {
                    tmp += char;
                }
            } else if (state == 'open_option') {
                if (char == '(') {
                    state = 'correct_option';
                    result += this.addOpenCorrectOption();
                } else if (char == ')') {
                    result += this.addOpenOption();
                    result += this.addOption(tmp);
                    result += this.addCloseOption();
                    state = 'text';
                    tmp = '';
                } else {
                    result += this.addOpenOption();
                    state = 'option';
                    tmp += char;
                }
            } else if (state == 'option') {
                if (char == ')') {
                    result += this.addOption(tmp);
                    result += this.addCloseOption();
                    state = 'text';
                    tmp = '';
                } else {
                    tmp += char;
                }
            } else if (state == 'correct_option') {
                if (char == ')') {
                    state = 'close_correct_option';
                } else {
                    tmp += char;
                }
            } else if (state == 'close_correct_option') {
                if (char == ')') {
                    result += this.addCorrectOption(tmp);
                    result += this.addCloseCorrectOption();
                    state = 'text';
                    tmp = '';
                } else {
                    state = 'correct_option';
                    tmp += ')' + char;
                }
            }
        }

        if (state == 'text' && tmp != '') {
            result += this.addText(tmp);
        } else if (state == 'open_option') {
            result += this.addOpenOption();
        } else if (state == 'option') {
            result += this.addOption(tmp);
        } else if (state == 'correct_option') {
            result += this.addCorrectOption(tmp);
        } else if (state == 'close_correct_option') {
            result += this.addCorrectOption(tmp + ')');
        }

        return result;
    }
};