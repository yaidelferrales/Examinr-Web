var ErrorReporter = function (container) {
    var self = this;
    this.element = container;
    this.items = container.find('ol');

    self.clean = function () {
        self.items.html('');
        self.element.css({display: 'none'});
    };

    self.report = function (text) {
        self.items.append('<li>' + text + '</li>');
        self.element.css({display: 'block'});
    };
};