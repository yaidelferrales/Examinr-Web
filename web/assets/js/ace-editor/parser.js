var Parser = function (editor, TokenIterator, selectCorrect, errorReporter) {
    var self = this;
    self.editor = editor;
    self.TokenIterator = TokenIterator;
    self.selectCorrect = selectCorrect || false;
    self.options = {};

    self.editor.on('change', function () {
        if (self.timer)
            clearTimeout(self.timer);
        setTimeout(function () {
            self.analize();
        }, 100);
    });

    self.analize = function () {
        var iterator = new self.TokenIterator(editor.getSession(), 0, 0);
        var errorsList = [];
        self.options = {};
        var tokens = [];

        if (errorReporter) {
            errorReporter.clean();
        }

        if (iterator.getCurrentToken() == null)
            return;

        var prevToken, token, rowNum = 0, col, textContainer = $(self.editor.container).find('.ace_text-layer'), error = false, missOrder = false, lastOptionDetected = false;

        while (token = iterator.getCurrentToken()) {
            if (rowNum != iterator.$row) {
                rowNum = iterator.$row;
                col = -1;
            }

            if (token.type != 'text') {
                col++;
            }

            var last = tokens.length;
            tokens.push({
                row: rowNum,
                col: col,
                token: JSON.parse(JSON.stringify(token))
            });

            if (tokens[last].token.type == 'correct_option_end' || tokens[last].token.type == 'option_end') {
                if (tokens[last - 1].token.type != 'correct_option' && tokens[last - 1].token.type != 'option') {
                    var errorToken = JSON.parse(JSON.stringify(tokens[last - 1]));
                    errorToken.errorType = 'Empty Option';
                    errorsList.push(errorToken);

                    var errorToken = JSON.parse(JSON.stringify(tokens[last]));
                    errorToken.errorType = 'Empty Option';
                    errorsList.push(errorToken);
                    if (errorReporter) {
                        if (tokens[last].token.type == 'correct_option_end')
                            errorReporter.report('Missing option around (())');
                        else
                            errorReporter.report('Missing option around ()');
                    }
                } else if (tokens[last - 1].token.value.trim() == '') {
                    var errorToken = JSON.parse(JSON.stringify(tokens[last - 2]));
                    errorToken.errorType = 'Empty Option';
                    errorsList.push(errorToken);

                    var errorToken = JSON.parse(JSON.stringify(tokens[last - 1]));
                    errorToken.errorType = 'Empty Option';
                    errorsList.push(errorToken);

                    var errorToken = JSON.parse(JSON.stringify(tokens[last]));
                    errorToken.errorType = 'Empty Option';
                    errorsList.push(errorToken);
                    if (errorReporter) {
                        if (tokens[last].token.type == 'correct_option_end')
                            errorReporter.report('Missing option around (())');
                        else
                            errorReporter.report('Missing option around ()');
                    }
                }
            }

            if (tokens[last].token.type == 'correct_option' || tokens[last].token.type == 'option') {
//                console.log(tokens[last].token.value);
//                console.log(self.options[tokens[last].token.value]);
                if (self.options[tokens[last].token.value]) {
                    if (self.options[tokens[last].token.value] == 1) {
                        errorReporter.report("The option '" + tokens[last].token.value + "' is repeated");
                        self.options[tokens[last].token.value] += 1;
                    } else {
                        self.options[tokens[last].token.value] += 1;
                    }
                } else {
                    self.options[tokens[last].token.value] = 1;
                }
            }

            //if ((tokens[last].token.type == 'correct_option_start' || tokens[last].token.type == 'option_start') && lastOptionDetected) {
            //    missOrder = true;
            //}

            if (tokens[last].token.type == 'text' && last > 0 && tokens[last-1].token.type != 'text' && tokens[last].token.value != '.') {
                missOrder = true;
                //lastOptionDetected = true;
            }

            iterator.stepForward();
        }

        //printing tokens
        //console.log(tokens);

        var last = tokens.length - 1;
        if ((tokens[last].token.type == 'correct_option_start' || tokens[last].token.type == 'option_start') && tokens.length > 0) {
            var errorToken = JSON.parse(JSON.stringify(tokens[last]));
            errorToken.errorType = 'Incomplete Option';
            errorsList.push(errorToken);
            if (errorReporter) {
                if (tokens[last].token.type == 'correct_option_start')
                    errorReporter.report('Incomplete correct option at the end of the question');
                else
                    errorReporter.report('Incomplete option at the end of the question');
            }
        } else if ((tokens[last].token.type == 'correct_option' || tokens[last].token.type == 'option') && tokens.length > 0) {
            var errorToken = JSON.parse(JSON.stringify(tokens[last - 1]));
            errorToken.errorType = 'Incomplete Option';
            errorsList.push(errorToken);

            var errorToken = JSON.parse(JSON.stringify(tokens[last]));
            errorToken.errorType = 'Incomplete Option';
            errorsList.push(errorToken);
            if (errorReporter) {
                if (tokens[last].token.type == 'correct_option')
                    errorReporter.report('Incomplete correct option at the end of the question');
                else
                    errorReporter.report('Incomplete option at the end of the question');
            }
        }

        if (missOrder && self.selectCorrect) {
            errorReporter.report('All options must be together');
        }

        var textContainer = $(self.editor.container).find('.ace_text-layer');
        for (var i = 0; i < errorsList.length; i++) {
            var row = $(textContainer.children()[errorsList[i].row]);
            if (row.children().length > errorsList[i].col) {
                item = $(row.children()[errorsList[i].col]).addClass('error');
            }
        }
    };

    self.setSelectCorrect = function (selectCorrect) {
        this.selectCorrect = selectCorrect;
    };

    setTimeout(function () {
        self.analize();
    }, 100);
};