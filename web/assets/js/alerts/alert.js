$(function () {
    $.alertSystem = {
        id: 1,
        alertContainer: undefined,
        show: function (type, msg) {
            if ($.alertSystem.alertContainer == undefined)
                $('body').append($.alertSystem.alertContainer = $('<div class="alert-area"></div>'));
            if (type == undefined)
                type = 'info';
            if (msg == undefined)
                msg = '';
            var id = 'alert-' + ($.alertSystem.id++);
            var alert = $('<div id="' + id + '" class="alert alert-' + type + ' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' + msg + '</div>');
            if ($.alertSystem.alertContainer.children().length > 0) {
                $($.alertSystem.alertContainer.children()[0]).before(alert);
            } else{
                $.alertSystem.alertContainer.append(alert);
            }
            setTimeout(function(){
                $.alertSystem.hide(id);
            },5000);
        },
        hide: function (id) {
            var alert = $('#' + id).remove();
        },
        hideAll: function () {
            if ($.alertSystem.alertContainer != undefined)
                $.alertSystem.alertContainer.html('');
        }
    }
});