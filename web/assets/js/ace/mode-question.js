define("ace/mode/question_highlight_rules", ["require", "exports", "module", "ace/lib/oop", "ace/mode/text_highlight_rules"], function (require, exports, module) {
    "use strict";

    var oop = require("../lib/oop");
    var TextHighlightRules = require("./text_highlight_rules").TextHighlightRules;

    var QuestionHighlightRules = function () {
        TextHighlightRules.call(this);
        var questionRules = {
            start: [
                { token: 'correct_option_start',
                    regex: '\\(\\(',
                    push: [
                        { token: 'correct_option_end',
                            regex: '\\)\\)',
                            next: 'pop' },
                        { defaultToken: 'correct_option' }
                    ]
                },
                { token: 'option_start',
                    regex: '\\(',
                    push: [
                        { token: 'option_end',
                            regex: '\\)',
                            next: 'pop' },
                        { defaultToken: 'option' }
                    ]
                }
            ]
        }

        var questionStart = questionRules.start;

        for (var rule in this.$rules) {
            this.$rules[rule].unshift.apply(this.$rules[rule], questionStart);
        }

        Object.keys(questionRules).forEach(function (x) {
            if (!this.$rules[x])
                this.$rules[x] = questionRules[x];
        }, this);

        this.normalizeRules();
    };

    oop.inherits(QuestionHighlightRules, TextHighlightRules);

    exports.QuestionHighlightRules = QuestionHighlightRules;
});

define("ace/mode/question", ["require", "exports", "module", "ace/lib/oop", "ace/mode/text", "ace/mode/question_highlight_rules"], function (require, exports, module) {
    "use strict";

    var oop = require("../lib/oop");
    var TextMode = require("./text").Mode;
    var QuestionHighlightRules = require("./question_highlight_rules").QuestionHighlightRules;

    var Mode = function () {
        this.HighlightRules = QuestionHighlightRules;
    };
    oop.inherits(Mode, TextMode);

    (function () {

        this.$id = "ace/mode/question";
    }).call(Mode.prototype);

    exports.Mode = Mode;
});