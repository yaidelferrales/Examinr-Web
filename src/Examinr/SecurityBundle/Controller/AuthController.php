<?php

namespace Examinr\SecurityBundle\Controller;

use Proxies\__CG__\Examinr\SecurityBundle\Entity\Login;
use Proxies\__CG__\Examinr\SecurityBundle\Entity\Student;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use ErrorException;

class AuthController extends Controller
{
    function loginAction (Request $request) {
        $em = $this->getDoctrine()->getManager();
        $lrepository = $em->getRepository('ExaminrSecurityBundle:Login');
        $srepository = $em->getRepository('ExaminrSecurityBundle:Student');

        if (!$this->isValidDNI($request->request->get('dni'))) {
            return new Response('Invalid identifier');
        }
        if (!$srepository->findOneByDni($request->request->get('dni'))) {
            $student = new Student();
            $student->setDni(trim($request->request->get('dni')));

            $em->persist($student);
            $em->flush();
        }
        $ip = $this->container->get('request')->getClientIp();
        $login = $lrepository->findOneByDni($request->request->get('dni'));
        if ($login && $login->getIp() !== $ip && round(microtime(true) * 1000) - $login->getLastAccess() < 3600000) {
            return new Response(json_encode(array('You are already logged in')));
        }

        if (!$login) {
            $login = new Login();
            $login->setDni(trim($request->request->get('dni')));
        }
        $login->setLastAccess(round(microtime(true) * 1000));
        $login->setIp($ip);

        $em->persist($login);
        $em->flush();
        return new Response(json_encode(array('Login ok')));
    }

    function logoutAction (Request $request) {
        $em = $this->getDoctrine()->getManager();
        $lrepository = $em->getRepository('ExaminrSecurityBundle:Login');

        if (!$this->isValidDNI($request->request->get('dni'))) {
            return new Response('Invalid identifier');
        }
        $login = $lrepository->findOneByDni($request->request->get('dni'));
        if ($login) {
            $em->remove($login);
            $em->flush();
        }
        return new Response(json_encode(array('Logout ok')));
    }

    function isAuthenticated($em, $dni) {
        $lrepository = $em->getRepository('ExaminrSecurityBundle:Login');

        if (!$this->isValidDNI($dni)) {
            throw new \ErrorException("Invalid identifier");
        }
        $login = $lrepository->findOneByDni($dni);
        return $login != null;
    }

    function isValidDNI ($dni) {
        return strlen($dni) == 10;
    }
}
