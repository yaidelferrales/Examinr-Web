<?php

namespace Examinr\SecurityBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\Request;

class UserRepository extends EntityRepository
{
    public function filter(Request $request, $deleted = 'false')
    {
        return array();
    }

    public function count($deleted = 'false')
    {
        $em = $this->getEntityManager();

        $qb = $em->createQuery("SELECT count(e.id) FROM ExaminrSecurityBundle:User e WHERE e.deleted = " . $deleted);

        return $qb->getSingleScalarResult();
    }
}