<?php

namespace Examinr\CoreBundle\Controller;

use Examinr\CoreBundle\Entity\Module;
use Examinr\CoreBundle\Entity\SurveyTopic;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SurveyTopicController extends Controller
{
    function indexAction(Request $request)
    {
        return $this->render('ExaminrCoreBundle:SurveyTopic:index.html.twig');
    }

    function ajaxListAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:SurveyTopic');

        $URI = $request->server->get('REQUEST_URI');
        $deleted = strpos($URI, '/deleted/') ? 'true' : 'false';

        $filterData = $repository->filter($request, $deleted);

        $result = array_merge($filterData, array(
            "draw" => $request->query->get('draw'),
            "recordsTotal" => $repository->count($deleted),
        ));

        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    function newAction(Request $request)
    {
        $data = array(
            'name' => $request->request->get('name'),
        );
//        var_dump($request);
        $name = trim($data['name']);
        if ($name) {
            $em = $this->getDoctrine()->getManager();
            $repository = $em->getRepository('ExaminrCoreBundle:SurveyTopic');
            $topic = $repository->findOneByName($name);
            if (!$topic) {

                $topic = new SurveyTopic();
                $topic->setName($name);
                $topic->setActive(true);
                $topic->setDeleted(false);

                $em->persist($topic);
                $em->flush();

                $data['id'] = $topic->getId();

            } else {
                $data['error'] = 'There is a Survey Topic with the name ' . $name . ' already.';
            }
        } else {
            $data['error'] = (strlen($name) == 0 ? 'empty' : 'null');
        }

        $response = new Response(json_encode($data));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    function setActiveAction (Request $request, $topic_id)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:SurveyTopic');

        $topic = $repository->findOneById($topic_id);

        if ($topic) {
            $topic->setActive(!$topic->getActive());

            $em->persist($topic);
            $em->flush();

            $data = array(
                'id' => $topic->getId()
            );

            $response = new Response(json_encode($data));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
        throw new NotFoundHttpException();
    }

    function editAction (Request $request, $topic_id)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:SurveyTopic');

        $topic = $repository->findOneById($topic_id);

        if ($topic) {
            $data = array(
                'name' => $request->request->get('name'),
            );
            $name = trim($data['name']);

            if ($name) {
                $topic1 = $repository->findOneByName($name);
                if (!$topic1) {
                    $topic->setName($name);

                    $em->persist($topic);
                    $em->flush();

                    $data = array(
                        'id' => $topic->getId()
                    );
                } else {
                    $data['error'] = 'There is a Survey Topic with that name already.';
                }
            } else {
                $data['error'] = (strlen($name) == 0 ? 'empty' : 'null');
            }

            $response = new Response(json_encode($data));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
        throw new NotFoundHttpException();
    }

    function getEditAction($topic_id)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:SurveyTopic');

        $topic = $repository->findOneById($topic_id);

        if ($topic) {
            return $this->render('@orgCore/Module/get_edit.html.twig', array(
                'module' => $topic
            ));
        }
        throw new NotFoundHttpException();
    }

    function deleteAction($topic_id)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:SurveyTopic');

        $topic = $repository->findOneById($topic_id);

        if ($topic) {
            $topic->setDeleted(true);

            $em->persist($topic);
            $em->flush();

            $data = array(
                'id' => $topic->getId()
            );

            $response = new Response(json_encode($data));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
        throw new NotFoundHttpException();
    }

    function deletedAction(Request $request)
    {
        return $this->render('ExaminrCoreBundle:Module:deleted.html.twig');
    }

    function undeleteAction($topic_id)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:SurveyTopic');

        $topic = $repository->findOneById($topic_id);

        if ($topic) {
            $topic->setDeleted(false);

            $em->persist($topic);
            $em->flush();

            $data = array(
                'id' => $topic->getId()
            );

            $response = new Response(json_encode($data));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
        throw new NotFoundHttpException();
    }
}
