<?php

namespace Examinr\CoreBundle\Controller;

use Examinr\CoreBundle\Entity\Exam;
use Examinr\CoreBundle\Entity\Module;
use Examinr\CoreBundle\Entity\Question;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Constraints\True;

class ExamController extends Controller
{
    function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:Module');

        return $this->render('ExaminrCoreBundle:Exam:index.html.twig', array(
            "modules" => $repository->allModules()
        ));
    }

    function ajaxListAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:Exam');

        $URI = $request->server->get('REQUEST_URI');
        $deleted = strpos($URI, '/deleted/') ? 'true' : 'false';

        $filterData = $repository->filter($request, $deleted);

        $result = array_merge($filterData, array(
            "draw" => $request->query->get('draw'),
            "recordsTotal" => $repository->count($deleted),
        ));

        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:Module');
        $modules = $repository->allModules();
        $model = array(
            "modules" => $modules
        );

        if ($request->getMethod() == 'POST') {
            $errors = array();
            $error = false;
            $exam = new Exam();

            $exam->setName(trim($request->request->get('name')));
            if (strlen($exam->getName()) == 0) {
                $error = true;
                $errors[] = "The name field can't be empty";
            }

            $exam->setModule($repository->findOneById($request->request->get('moduleFilter')));
            if (!$exam->getModule()) {
                $errors[] = "The module can't be null";
            }

            $exam->setDuration(trim($request->request->get('duration')));
            if (sizeof($exam->getDuration()) == 0) {
                $errors[] = "The duration field can't be empty";
            } else {
                $parts = explode(':', $exam->getDuration());
                if (sizeof($parts) != 3) {
                    $errors[] = "The duration has an incorrect format";
                } else if (!is_numeric(trim($parts[0]))) {
                    $errors[] = "The duration has an incorrect format";
                } else if (!is_numeric(trim($parts[1]))) {
                    $errors[] = "The duration has an incorrect format";
                } else if (!is_numeric(trim($parts[2]))) {
                    $errors[] = "The duration has an incorrect format";
                } else {
                    $duration = 0;
                    $duration += intval(trim($parts[2]));
                    $duration += (intval(trim($parts[1])) * 60);
                    $duration += (intval(trim($parts[0])) * 3600);
                    $exam->setDuration($duration);
                }
            }

            $exam->setBase(trim($request->request->get('base')));
            if (sizeof($exam->getBase()) == 0) {
                $error = true;
                $errors[] = "The Grade over field can't be empty";
            } else if (!is_numeric(trim($exam->getBase()))) {
                $error = true;
                $errors[] = "The Grade over field only accepts positive numbers";
            }

            $temp = trim($request->request->get('questionsCc'));
            $temp = sizeof($temp) == 0 ? 0 : $temp;
            $exam->setCantQuestionsCc($temp);
            if (!is_numeric($temp)) {
                $error = true;
                $errors[] = "The correct questions field only accepts positive numbers";
            } else if (strpos($temp, '-') !== false) {
                $error = true;
                $errors[] = "The correct questions field only accepts positive numbers";
            } else {
                $cant = $repository->cantQuestions(1, true);
                if ($cant < $temp) {
                    $error = true;
                    $errors[] = "The correct questions can't be more than " . $cant;
                }
            }

            $temp = trim($request->request->get('questionsCi'));
            $temp = sizeof($temp) == 0 ? 0 : $temp;
            $exam->setCantQuestionsCi($temp);
            if (!is_numeric($temp)) {
                $error = true;
                $errors[] = "The incorrect questions field only accepts positive numbers";
            } else if (strpos($temp, '-') !== false) {
                $error = true;
                $errors[] = "The incorrect questions field only accepts positive numbers";
            } else {
                $cant = $repository->cantQuestions(1, false);
                if ($cant < $temp) {
                    $error = true;
                    $errors[] = "The incorrect questions can't be more than " . $cant;
                }
            }

            if (!$error && $exam->getModule() && $exam->getCantQuestionsCi() + $exam->getCantQuestionsCc() < 1) {
                $errors[] = "The sum of correct question and incorrect question has to be at least 1";
            }

            $exam->setActive($request->request->get('active') == 'on');
            $exam->setRandomizeQuestions($request->request->get('randomize') == 'on');

            if (sizeof($errors) == 0) {
                try {
                    $em->persist($exam);
                    $em->flush();

                    return $this->redirect($this->generateUrl('exams'));
                } catch (\Exception $ex) {
                    $errors[] = $ex->getMessage();
                }
            }

            if (sizeof($errors) > 0) {
                $model['exam'] = $exam;
                $model['errors'] = $errors;
            }
        }

        return $this->render('ExaminrCoreBundle:Exam:new_edit.html.twig', $model);
    }

    function editAction(Request $request, $exam_id)
    {
        $em = $this->getDoctrine()->getManager();
        $mRrepository = $em->getRepository('ExaminrCoreBundle:Module');
        $eRepository = $em->getRepository('ExaminrCoreBundle:Exam');
        $modules = $mRrepository->allModules();
        $model = array(
            "modules" => $modules
        );

        $exam = $eRepository->findOneById($exam_id);

        if (!$exam) {
            throw new NotFoundHttpException();
        }
        $model ['exam'] = $exam;

        if ($request->getMethod() == 'POST') {
            $errors = array();
            $error = false;

            $exam->setName(trim($request->request->get('name')));
            if (strlen($exam->getName()) == 0) {
                $error = true;
                $errors[] = "The name field can't be empty";
            }

            $exam->setModule($mRrepository->findOneById($request->request->get('moduleFilter')));
            if (!$exam->getModule()) {
                $errors[] = "The module can't be null";
            }

            $exam->setDuration(trim($request->request->get('duration')));
            if (sizeof($exam->getDuration()) == 0) {
                $errors[] = "The duration field can't be empty";
            } else {
                $parts = explode(':', $exam->getDuration());
                if (sizeof($parts) != 3) {
                    $errors[] = "The duration has an incorrect format";
                } else if (!is_numeric(trim($parts[0]))) {
                    $errors[] = "The duration has an incorrect format";
                } else if (!is_numeric(trim($parts[1]))) {
                    $errors[] = "The duration has an incorrect format";
                } else if (!is_numeric(trim($parts[2]))) {
                    $errors[] = "The duration has an incorrect format";
                } else {
                    $duration = 0;
                    $duration += intval(trim($parts[2]));
                    $duration += (intval(trim($parts[1])) * 60);
                    $duration += (intval(trim($parts[0])) * 3600);
                    $exam->setDuration($duration);
                }
            }

            $exam->setBase(trim($request->request->get('base')));
            if (sizeof($exam->getBase()) == 0) {
                $error = true;
                $errors[] = "The Grade over field can't be empty";
            } else if (!is_numeric(trim($exam->getBase()))) {
                $error = true;
                $errors[] = "The Grade over field only accepts positive numbers";
            }

            $temp = trim($request->request->get('questionsCc'));
            $temp = sizeof($temp) == 0 ? 0 : $temp;
            $exam->setCantQuestionsCc($temp);
            if (!is_numeric($temp)) {
                $error = true;
                $errors[] = "The correct questions field only accepts positive numbers";
            } else if (strpos($temp, '-') !== false) {
                $error = true;
                $errors[] = "The correct questions field only accepts positive numbers";
            } else {
                $cant = $mRrepository->cantQuestions(1, true);
                if ($cant < $temp) {
                    $error = true;
                    $errors[] = "The correct questions can't be more than " . $cant;
                }
            }

            $temp = trim($request->request->get('questionsCi'));
            $temp = sizeof($temp) == 0 ? 0 : $temp;
            $exam->setCantQuestionsCi($temp);
            if (!is_numeric($temp)) {
                $error = true;
                $errors[] = "The incorrect questions field only accepts positive numbers";
            } else if (strpos($temp, '-') !== false) {
                $error = true;
                $errors[] = "The incorrect questions field only accepts positive numbers";
            } else {
                $cant = $mRrepository->cantQuestions(1, false);
                if ($cant < $temp) {
                    $error = true;
                    $errors[] = "The incorrect can't be more than " . $cant;
                }
            }

            if (!$error && $exam->getModule() && $exam->getCantQuestionsCi() + $exam->getCantQuestionsCc() < 1) {
                $errors[] = "The sum of correct question and incorrect question has to be at least 1";
            }

            $exam->setActive($request->request->get('active') == 'on');
            $exam->setRandomizeQuestions($request->request->get('randomize') == 'on');

            if (sizeof($errors) == 0) {
                try {
                    $em->persist($exam);
                    $em->flush();

                    return $this->redirect($this->generateUrl('exams'));
                } catch (\Exception $ex) {
                    $errors[] = $ex->getMessage();
                }
            }

            if (sizeof($errors) > 0) {
                $model['exam'] = $exam;
                $model['errors'] = $errors;
            }
        }

        return $this->render('ExaminrCoreBundle:Exam:new_edit.html.twig', $model);
    }

    function setActiveAction($exam_id)
    {

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:Exam');

        $exam = $repository->findOneById($exam_id);

        if ($exam) {
            $exam->setActive(!$exam->getActive());

            $em->persist($exam);
            $em->flush();

            $data = array(
                'id' => $exam->getId()
            );

            $response = new Response(json_encode($data));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
        throw new NotFoundHttpException();
    }

    function deleteAction($exam_id)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:Exam');

        $exam = $repository->findOneById($exam_id);

        if ($exam) {
            $exam->setDeleted(true);

            $em->persist($exam);
            $em->flush();

            $data = array(
                'id' => $exam->getId()
            );

            $response = new Response(json_encode($data));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
        throw new NotFoundHttpException();
    }

    function deletedAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:Module');

        return $this->render('ExaminrCoreBundle:Exam:deleted.html.twig', array(
            "modules" => $repository->allModules()
        ));
    }

    function undeleteAction($exam_id)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:Exam');

        $exam = $repository->findOneById($exam_id);

        if ($exam) {
            $exam->setDeleted(false);

            $em->persist($exam);
            $em->flush();

            $data = array(
                'id' => $exam->getId()
            );

            $response = new Response(json_encode($data));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
        throw new NotFoundHttpException();
    }

    function validateQuestion($question)
    {
        $correctOptionsCount = 0;
        $options = array();
        $optionsCount = 0;
        $state = "text";
        $text = '';
        $tmp = "";

        for ($i = 0; $i < strlen($question); $i++) {
            $char = $question[$i];

            if ($state == 'text') {
                if ($char == '[') {
                    $state = 'open_option';
                } else {
                    $text .= $char;
                }
            } else if ($state == 'open_option') {
                if ($char == '[') {
                    $state = 'correct_option';
                } else if ($char == ']') {
                    return false;
                } else {
                    $state = 'option';
                    $tmp .= $char;
                }
            } else if ($state == 'option') {
                if ($char == ']') {
                    if (strlen(trim($tmp)) == 0)
                        return false;
                    if (isset($options[trim($tmp)]))
                        return false;
                    $options[trim($tmp)] = 1;
                    $optionsCount++;
                    $state = 'text';
                    $tmp = '';
                } else {
                    $tmp .= $char;
                }
            } else if ($state == 'correct_option') {
                if ($char == ']') {
                    $state = 'close_correct_option';
                } else {
                    $tmp .= $char;
                }
            } else if ($state == 'close_correct_option') {
                if ($char == ']') {
                    if (strlen(trim($tmp)) == 0)
                        return false;
                    if (isset($options[trim($tmp)]))
                        return false;
                    $options[trim($tmp)] = 1;
                    $correctOptionsCount++;
                    $optionsCount++;
                    $state = 'text';
                    $tmp = '';
                } else {
                    $state = 'correct_option';
                    $tmp .= ']' . $char;
                }
            }
        }

        return ($optionsCount > 1 && $correctOptionsCount == 1 && $state == 'text');
    }
}
