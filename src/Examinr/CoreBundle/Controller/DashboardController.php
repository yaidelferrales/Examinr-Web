<?php

namespace Examinr\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DashboardController extends Controller
{
    public function indexAction()
    {
        return $this->redirect($this->generateUrl('modules'));
    }
}
