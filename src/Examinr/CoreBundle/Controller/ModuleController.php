<?php

namespace Examinr\CoreBundle\Controller;

use Examinr\CoreBundle\Entity\Module;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ModuleController extends Controller
{
    function indexAction(Request $request)
    {
        return $this->render('ExaminrCoreBundle:Module:index.html.twig');
    }

    function ajaxListAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:Module');

        $URI = $request->server->get('REQUEST_URI');
        $deleted = strpos($URI, '/deleted/') ? 'true' : 'false';

        $filterData = $repository->filter($request, $deleted);

        $result = array_merge($filterData, array(
            "draw" => $request->query->get('draw'),
            "recordsTotal" => $repository->count($deleted),
        ));

        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    function newAction(Request $request)
    {
        $data = array(
            'name' => $request->request->get('name'),
        );
//        var_dump($request);
        $name = trim($data['name']);
        if ($name) {
            $em = $this->getDoctrine()->getManager();
            $repository = $em->getRepository('ExaminrCoreBundle:Module');
            $module = $repository->findOneByName($name);
            if (!$module) {

                $module = new Module();
                $module->setName($name);
                $module->setActive(true);
                $module->setDeleted(false);

                $em->persist($module);
                $em->flush();

                $data['id'] = $module->getId();

            } else {
                $data['error'] = 'There is a module with that name already.';
            }
        } else {
            $data['error'] = (strlen($name) == 0 ? 'empty' : 'null');
        }

        $response = new Response(json_encode($data));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    function setActiveAction (Request $request, $module_id)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:Module');

        $module = $repository->findOneById($module_id);

        if ($module) {
            $module->setActive(!$module->getActive());

            $em->persist($module);
            $em->flush();

            $data = array(
                'id' => $module->getId()
            );

            $response = new Response(json_encode($data));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
        throw new NotFoundHttpException();
    }

    function editAction (Request $request, $module_id)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:Module');

        $module = $repository->findOneById($module_id);

        if ($module) {
            $data = array(
                'name' => $request->request->get('name'),
            );
            $name = trim($data['name']);

            if ($name) {
                $module1 = $repository->findOneByName($name);
                if (!$module1) {
                    $module->setName($name);

                    $em->persist($module);
                    $em->flush();

                    $data = array(
                        'id' => $module->getId()
                    );
                } else {
                    $data['error'] = 'There is a module with that name already.';
                }
            } else {
                $data['error'] = (strlen($name) == 0 ? 'empty' : 'null');
            }

            $response = new Response(json_encode($data));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
        throw new NotFoundHttpException();
    }

    function getEditAction($module_id)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:Module');

        $module = $repository->findOneById($module_id);

        if ($module) {
            return $this->render('@orgCore/Module/get_edit.html.twig', array(
                'module' => $module
            ));
        }
        throw new NotFoundHttpException();
    }

    function deleteAction($module_id)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:Module');

        $module = $repository->findOneById($module_id);

        if ($module) {
            $module->setDeleted(true);

            $em->persist($module);
            $em->flush();

            $data = array(
                'id' => $module->getId()
            );

            $response = new Response(json_encode($data));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
        throw new NotFoundHttpException();
    }

    function deletedAction(Request $request)
    {
        return $this->render('ExaminrCoreBundle:Module:deleted.html.twig');
    }

    function undeleteAction($module_id)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:Module');

        $module = $repository->findOneById($module_id);

        if ($module) {
            $module->setDeleted(false);

            $em->persist($module);
            $em->flush();

            $data = array(
                'id' => $module->getId()
            );

            $response = new Response(json_encode($data));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
        throw new NotFoundHttpException();
    }
}
