<?php

namespace Examinr\CoreBundle\Controller;

use Examinr\CoreBundle\Entity\SurveyAnswer;
use Examinr\CoreBundle\Entity\SurveyAnswerQuestion;
use Examinr\CoreBundle\Helpers\RestClient;
use Examinr\CoreBundle\Entity\ExamAnswer;
use Examinr\CoreBundle\Entity\ExamAnswerQuestion;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use GuzzleHttp\Client;

class ApiController extends Controller
{
    public function listFunctionsAction()
    {
        $router = $this->get("router");

        $result = array(
            "functions" => array(
                "baseUrl" => $this->generateUrl("api_functions", array(), true),
                "loginUrl" => $this->generateUrl("student_login", array(), true),
                "logoutUrl" => $this->generateUrl("student_logout", array(), true),
                "listQuestionnairesUrl" => $this->generateUrl("api_exams", array(), true),
                "listQuestionsUrl" => $this->generateUrl("api_questions", array(), true),
                "listQuestionsSurveyUrl" => $this->generateUrl("api_questions_survey", array(), true),
                "examReceiverUrl" => $this->generateUrl("api_exam_receiver", array(), true),
                "surveyReceiverUrl" => $this->generateUrl("api_survey_receiver", array(), true)
            )
        );
        return new Response(json_encode($result));
    }

    /**
     * Lists all exams and surveys available
     *
     * @param Request $request
     * @return Response
     */
    public function listQuestionnairesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $securityBase = $this->get("examinr_security.securityActions");
        $dni = $request->request->get('dni');

        try {
            if (!$securityBase->isAuthenticated($em, $dni)) {
                return new Response('The specified user is not logged in', 403);
            }
        } catch (\ErrorException $ex) {
            if ($ex->getMessage() == "Invalid identifier") {
                return new Response('Invalid identifier');
            } else {
                return new Response('The specified user is not logged in', 403);
            }
        }

        $repository = $em->getRepository('ExaminrCoreBundle:Exam');

        $exams = $repository->allStudentHaventDone($dni);
        $result = array();

        foreach ($exams as $exam) {
            $result[] = array(
                "id" => $exam->getId(),
                "type" => "exam",
                "name" => $exam->getName(),
                "duration" => $exam->getDuration(),
            );
        }

        $repository = $em->getRepository('ExaminrCoreBundle:Survey');

        $exams = $repository->allStudentHaventDone($dni);

        foreach ($exams as $exam) {
            $result[] = array(
                "id" => $exam->getId(),
                "type" => 'survey',
                "name" => $exam->getName(),
            );
        }

        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function generateExamAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $securityBase = $this->get("examinr_security.securityActions");
        $dni = $request->request->get('dni');

        try {
            if (!$securityBase->isAuthenticated($em, $dni)) {
                return new Response('The specified user is not logged in', 403);
            }
        } catch (\ErrorException $ex) {
            if ($ex->getMessage() == "Invalid identifier") {
                return new Response('Invalid identifier');
            } else {
                return new Response('The specified user is not logged in', 403);
            }
        }

        $erepository = $em->getRepository('ExaminrCoreBundle:Exam');
        $exam_id = $request->request->get('exam');
        $exam = $erepository->findOneById($exam_id);

        if (!$exam || !$exam->getActive() || $exam->getDeleted() || !$exam->getModule()->getActive() || $exam->getModule()->getDeleted()) {
            return new Response('The specified exam does not exist', 404);
        }

        $srepository = $em->getRepository('ExaminrSecurityBundle:Student');
        $student = $srepository->findOneByDni($dni);

        $earepository = $em->getRepository('ExaminrCoreBundle:ExamAnswer');
        $examAnswer = $earepository->findByStudentAndExam($student, $exam);
        if ($examAnswer) {
            return new Response('You already took this exam', 403);
        }

        $examAnswer = new ExamAnswer();
        $examAnswer->student = $student;
        $examAnswer->exam = $exam;

        $em->persist($examAnswer);

        $qrepository = $em->getRepository('ExaminrCoreBundle:Question');
        $questions = array();

        if ($exam->getRandomizeQuestions() == true) {
            //select the correct questions
            $ids = $qrepository->getByModuleANDType($exam->getModule()->getId(), "Choose Correct");

            for ($i = 0; $i < $exam->getCantQuestionsCc() && sizeof($ids) > 0; $i++) {
                $index = rand() % sizeof($ids);
                $question = $qrepository->findOneById($ids[$index]["id"]);
                if ($question) {
                    $questions[] = array(
                        "id" => $question->getId(),
                        "type" => 1,
                        "text" => $question->getText()
                    );
                    $examAnswerQuestion = new ExamAnswerQuestion();
                    $examAnswerQuestion->examAnswer = $examAnswer;
                    $examAnswerQuestion->question = $question;
                    $em->persist($examAnswerQuestion);
                }
                array_splice($ids, $index, 1);
            }

            //select the incorrect questions
            $ids = $qrepository->getByModuleANDType($exam->getModule()->getId(), "Choose Incorrect");

            for ($i = 0; $i < $exam->getCantQuestionsCc() && sizeof($ids) > 0; $i++) {
                $index = rand() % sizeof($ids);
                $question = $qrepository->findOneById($ids[$index]["id"]);
                if ($question) {
                    $questions[] = array(
                        "id" => $question->getId(),
                        "type" => 0,
                        "text" => $question->getText()
                    );
                    $examAnswerQuestion = new ExamAnswerQuestion();
                    $examAnswerQuestion->examAnswer = $examAnswer;
                    $examAnswerQuestion->question = $question;
                    $em->persist($examAnswerQuestion);
                }
                array_splice($ids, $index, 1);
            }
        } else {
            //select the correct questions
            $ids = $qrepository->getByModuleANDType($exam->getModule()->getId(), "Choose Correct");

            for ($i = 0; $i < $exam->getCantQuestionsCc() && sizeof($ids) > 0; $i++) {
                $question = $qrepository->findOneById($ids[$i]["id"]);
                if ($question) {
                    $questions[] = array(
                        "id" => $question->getId(),
                        "type" => 1,
                        "text" => $question->getText()
                    );
                    $examAnswerQuestion = new ExamAnswerQuestion();
                    $examAnswerQuestion->examAnswer = $examAnswer;
                    $examAnswerQuestion->question = $question;
                    $em->persist($examAnswerQuestion);
                }
            }

            //select the incorrect questions
            $ids = $qrepository->getByModuleANDType($exam->getModule()->getId(), "Choose Incorrect");

            for ($i = 0; $i < $exam->getCantQuestionsCc() && sizeof($ids) > 0; $i++) {
                $question = $qrepository->findOneById($ids[$i]["id"]);
                if ($question) {
                    $questions[] = array(
                        "id" => $question->getId(),
                        "type" => 0,
                        "text" => $question->getText()
                    );
                    $examAnswerQuestion = new ExamAnswerQuestion();
                    $examAnswerQuestion->examAnswer = $examAnswer;
                    $examAnswerQuestion->question = $question;
                    $em->persist($examAnswerQuestion);
                }
            }
        }

        $em->flush();

        $message = "";
        if (null == $examAnswer->getId()) {
            $message = "Request with dni:" . $dni . ", exam:" . $exam_id . " failed to save the ExamAnswer" . "\r\n";
        } else {
            $message = "Request with dni:" . $dni . ", exam:" . $exam_id . " Generated ExamAnswer: " . $examAnswer->getId() . "\r\n";
        }
        $fp = fopen($this->get('kernel')->getRootDir() . "/../logs/error.log", "a+");
        fwrite($fp, $message);
        fclose($fp);

        $response = new Response(json_encode($questions));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }



    public function generateSurveyAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $securityBase = $this->get("examinr_security.securityActions");
        $dni = $request->request->get('dni');

        try {
            if (!$securityBase->isAuthenticated($em, $dni)) {
                return new Response('The specified user is not logged in', 403);
            }
        } catch (\ErrorException $ex) {
            if ($ex->getMessage() == "Invalid identifier") {
                return new Response('Invalid identifier');
            } else {
                return new Response('The specified user is not logged in', 403);
            }
        }

        $erepository = $em->getRepository('ExaminrCoreBundle:Survey');
        $survey_id = $request->request->get('survey');
        $survey = $erepository->findOneById($survey_id);

        if (!$survey || !$survey->getActive() || $survey->getDeleted() || !$survey->getTopic()->getActive() || $survey->getTopic()->getDeleted()) {
            return new Response('The specified exam does not exist', 404);
        }

        $srepository = $em->getRepository('ExaminrSecurityBundle:Student');
        $student = $srepository->findOneByDni($dni);

        $earepository = $em->getRepository('ExaminrCoreBundle:SurveyAnswer');
        $surveyAnswer = $earepository->findByStudentAndSurvey($student, $survey);
        if ($surveyAnswer) {
            return new Response('You already took this survey', 403);
        }

        $surveyAnswer = new SurveyAnswer();
        $surveyAnswer->student = $student;
        $surveyAnswer->survey = $survey;

        $em->persist($surveyAnswer);

        $qrepository = $em->getRepository('ExaminrCoreBundle:SurveyQuestion');
        $questions = array();

        $ids = $qrepository->getByTopic($survey->getTopic()->getId());
        $generated = 0;

        $total = sizeof($ids);
        while ($generated < $total) {
            $index = 0;
            if ($survey->getRandomizeQuestions() == true) {
                $index = rand() % sizeof($ids);
            }
            $question = $qrepository->findOneById($ids[$index]["id"]);
            if ($question) {
                $questions[] = array(
                    "id" => $question->getId(),
                    "type" => 1,
                    "text" => $question->getText()
                );
                $surveyAnswerQuestion = new SurveyAnswerQuestion();
                $surveyAnswerQuestion->surveyAnswer = $surveyAnswer;
                $surveyAnswerQuestion->question = $question;
                $em->persist($surveyAnswerQuestion);
            }
            array_splice($ids, $index, 1);

            $generated += 1;
        }
        $em->flush();

        $message = "";
        if (null == $surveyAnswer->getId()) {
            $message = "Request with dni:" . $dni . ", exam:" . $survey_id . " failed to save the ExamAnswer" . "\r\n";
        } else {
            $message = "Request with dni:" . $dni . ", survey:" . $survey_id . " Generated SurveyAnswer: " . $surveyAnswer->getId() . "\r\n";
        }
        $fp = fopen($this->get('kernel')->getRootDir() . "/../logs/error.log", "a+");
        fwrite($fp, $message);
        fclose($fp);

        $response = new Response(json_encode($questions));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function receiveExamAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $securityBase = $this->get("examinr_security.securityActions");
        $dni = $request->request->get('dni');

        if (!$securityBase->isValidDNI($dni))
            return new Response('Invalid identifier');

        $erepository = $em->getRepository('ExaminrCoreBundle:Exam');
        $exam_id = $request->request->get('exam');
        $exam = $erepository->findOneById($exam_id);

        if (!$exam) {
            return new Response('The specified exam does not exist', 404);
        }

        if (!$exam->getActive() || $exam->getDeleted() || !$exam->getModule()->getActive() || $exam->getModule()->getDeleted()) {
            return new Response('The specified exam is not active', 404);
        }

        $srepository = $em->getRepository('ExaminrSecurityBundle:Student');
        $student = $srepository->findOneByDni($dni);

        $earepository = $em->getRepository('ExaminrCoreBundle:ExamAnswer');
        $examAnswer = $earepository->findByStudentAndExam($student, $exam);
        if (!$examAnswer) {
            return new Response("The specified student is not doing the specified exam", 404);
        } else if ($examAnswer->finished) {
            return new Response("The specified student allready finished the specified exam", 403);
        }

        $eaqrepository = $em->getRepository('ExaminrCoreBundle:ExamAnswerQuestion');
        $questions = $eaqrepository->findByExamAnswer($examAnswer);

        $answers = json_decode(utf8_encode($request->request->get('answers')));
        $answersIndexed = array();
        for ($i = 0; $i < sizeof($answers); $i++) {
            $answersIndexed[$answers[$i]->id] = $answers[$i];
        }

        $correct = 0;
        foreach ($questions as $question) {
            if (isset($answersIndexed[$question->question->getId() . ""])) {
                $answer = $answersIndexed[$question->question->getId() . ""];
                $question->answer = $answer->answer;
                $question->correct = $answer->correct == "true";
                $correct += $question->correct ? 1 : 0;
                $em->persist($question);
            }
        }

        $examAnswer->correct = $correct;
        $examAnswer->finished = true;
        $em->persist($examAnswer);
        $em->flush();

        $this->sendGradeToListeningServers($examAnswer);

        $response = new Response("[\"Exam received\"]", 200);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function receiveSurveyAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $securityBase = $this->get("examinr_security.securityActions");
        $dni = $request->request->get('dni');

        if (!$securityBase->isValidDNI($dni))
            return new Response('Invalid identifier');

        $erepository = $em->getRepository('ExaminrCoreBundle:Survey');
        $survey_id = $request->request->get('survey');
        $survey = $erepository->findOneById($survey_id);

        if (!$survey) {
            return new Response('The specified survey does not exist', 404);
        }

        if (!$survey->getActive() || $survey->getDeleted() || !$survey->getTopic()->getActive() || $survey->getTopic()->getDeleted()) {
            return new Response('The specified survey is not active', 404);
        }

        $srepository = $em->getRepository('ExaminrSecurityBundle:Student');
        $student = $srepository->findOneByDni($dni);

        $earepository = $em->getRepository('ExaminrCoreBundle:SurveyAnswer');
        $surveyAnswer = $earepository->findByStudentAndSurvey($student, $survey);
        if (!$surveyAnswer) {
            return new Response("The specified student is not doing the specified survey", 404);
        } else if ($surveyAnswer->finished) {
            return new Response("The specified student allready finished the specified survey", 403);
        }

        $eaqrepository = $em->getRepository('ExaminrCoreBundle:SurveyAnswerQuestion');
        $questions = $eaqrepository->findBySurveyAnswer($surveyAnswer);

        $answers = json_decode(utf8_encode($request->request->get('answers')));
        $answersIndexed = array();
        for ($i = 0; $i < sizeof($answers); $i++) {
            $answersIndexed[$answers[$i]->id] = $answers[$i];
        }

        $correct = 0;
        foreach ($questions as $question) {
            if (isset($answersIndexed[$question->question->getId() . ""])) {
                $answer = $answersIndexed[$question->question->getId() . ""];
                $question->answer = $answer->answer;
                $em->persist($question);
            }
        }

        $surveyAnswer->finished = true;
        $em->persist($surveyAnswer);
        $em->flush();

        $response = new Response("[\"Survey received\"]", 200);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    //Export information section

    public function sendGradeToListeningServers(ExamAnswer $examAnswer)
    {
        try {
//            $client = new Client([
//                'base_uri' => 'http://localhost:4150/',
//                'timeout' => 2.0,
//            ]);
//            $response = $client->post('api/salvarNota', [
//                'form_params' => $examAnswer->jsonSerializeForServer()
//            ]);
        } catch (Exception $e) {}
    }

    public function listExamsForServerAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $repository = $em->getRepository('ExaminrCoreBundle:Exam');

        $exams = $repository->allExams();
        $result = array();

        foreach ($exams as $exam) {
            $result[] = array(
                "id" => $exam->getId(),
                "name" => $exam->getName(),
                "duration" => $exam->getDuration(),
                "base" => $exam->getBase()
            );
        }

        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function listExamResultsForServerAction(Request $request, $exam_id)
    {
        $em = $this->getDoctrine()->getManager();

        $repository = $em->getRepository('ExaminrCoreBundle:ExamAnswer');

        $examResults = $repository->findByExam($exam_id);
        $result = array();

        foreach ($examResults as $examResult) {
            $result[] = $examResult->jsonSerializeForServer();
        }

        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');

//        $api = new RestClient(array(
//            'base_url' => "http://localhost/examinr/web/app_dev.php/api",
//            'format' => "json"
//        ));
//
//        $result = $api->get("examsList/", array());
//
//        if($result->info->http_code == 200)
//            var_dump($result->decode_response());

        return $response;
    }
}

///examinr/web/app_dev.php/api/questions