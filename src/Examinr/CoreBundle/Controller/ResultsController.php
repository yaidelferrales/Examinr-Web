<?php

namespace Examinr\CoreBundle\Controller;

use Examinr\CoreBundle\Entity\Module;
use Examinr\CoreBundle\Entity\Question;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

class ResultsController extends Controller
{
    function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:Exam');

        return $this->render('ExaminrCoreBundle:Results:index.html.twig', array(
            "exams" => $repository->allExams()
        ));
    }

    function ajaxListAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:ExamAnswer');

        $filterData = $repository->filter($request, false);

        $result = array_merge($filterData, array(
            "draw" => $request->query->get('draw'),
            "recordsTotal" => $repository->count(false),
        ));

        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    function showAction($exam_answer_id)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:ExamAnswer');

        $examAnswer = null;
        try {
            $examAnswer = $repository->findOneById($exam_answer_id);
        } catch (Exception $e) {
        }
        if (!$examAnswer) {
            return new NotFoundHttpException();
        }

        return $this->render('ExaminrCoreBundle:Results:show.html.twig', array(
            "examAnswer" => $examAnswer
        ));
    }

    function invalidateAction($exam_answer_id)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ExaminrCoreBundle:ExamAnswer');

        $examAnswer = null;
        try {
            $examAnswer = $repository->findOneById($exam_answer_id);
        } catch (Exception $e) {
        }
        if (!$examAnswer) {
            return new NotFoundHttpException();
        }

        $em->remove($examAnswer);
        $em->flush();

        $response = new Response("");
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
