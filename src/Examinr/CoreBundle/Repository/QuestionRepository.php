<?php

namespace Examinr\CoreBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\Request;

class QuestionRepository extends EntityRepository
{
    public function filter(Request $request, $deleted = 'false')
    {
        $em = $this->getEntityManager();

        $start = is_numeric($request->query->get('start')) ? intval($request->query->get('start')) : 0;

        $lenght = is_numeric($request->query->get('length')) ? intval($request->query->get('length')) : 10;
//        if (!$start % $lenght != 0) {
//            $start = 0;
//        }

        $search = $request->query->get('search')['value'];

        $sortColumn = strtolower($request->query->get('order')[0]['column']);
        $sortColumn = $request->query->get('columns')[intval($sortColumn)]['name'];
        if ($sortColumn != 'text' && $sortColumn != 'type' && $sortColumn != 'module') {
            $sortColumn = 'text';
        }

        $sortOrder = isset($request->query->get('order')[0]['dir']) ? $request->query->get('order')[0]['dir'] : 'asc';
        if ($sortOrder != 'asc' && $sortOrder != 'desc') {
            $sortOrder = 'asc';
        }

        $module = $request->query->get('columns')[0]['search']['value'];
        $type = $request->query->get('columns')[2]['search']['value'];

//        var_dump($module);
//        var_dump($type);

        $recordsFilteredCount = $em->createQuery("SELECT COUNT(e) FROM ExaminrCoreBundle:Question e INNER JOIN e.module m WHERE e.text LIKE " . "'%" . $search . "%' " . (strlen($module) != 0 ? "AND e.module = '" . $module . "'" : "") . " " . (strlen($type) != 0 ? "AND e.type = '" . $type . "'" : '') . " AND e.deleted = " . $deleted . " AND m.deleted = false")->getSingleScalarResult();
        if ($start == $recordsFilteredCount) {
            $start -= $start == 0 ? 0 : $lenght;
        }

        $recordsFilteredItems = $em->createQuery("SELECT e FROM ExaminrCoreBundle:Question e INNER JOIN e.module m WHERE e.text LIKE " . "'%" . $search . "%' " . (strlen($module) != 0 ? "AND e.module = '" . $module . "'" : "") . " " . (strlen($type) != 0 ? "AND e.type = '" . $type . "'" : '') . " AND e.deleted = " . $deleted . " AND m.deleted = false ORDER BY " . ($sortColumn == 'module' ? "m.name" : ("e." . $sortColumn)) . " " . $sortOrder)
            ->setFirstResult($start)
            ->setMaxResults($lenght)
            ->getResult();

//        //CONCAT(d.estudiante_id, '') LIKE '%3%'

        return array(
            "recordsFiltered" => $recordsFilteredCount,
            "data" => $recordsFilteredItems
        );
    }

    public function count($deleted = 'false')
    {
        $em = $this->getEntityManager();

        $qb = $em->createQuery("SELECT count(e.id) FROM ExaminrCoreBundle:Question e WHERE e.deleted = " . $deleted);

        return $qb->getSingleScalarResult();
    }

    public function getOne($module, $type, $index)
    {
        $em = $this->getEntityManager();

        $questions = $em->createQuery("SELECT e FROM ExaminrCoreBundle:Question e INNER JOIN e.module m WHERE m.id = '" . $module . "' AND e.type = '" . $type . "' ORDER BY e.id")
            ->setFirstResult($index)
            ->setMaxResults(1)
            ->getResult();

        if (sizeof($questions) > 0)
            return$questions[0];
        return null;
    }

    public function countByModuleANDType($module, $type)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQuery("SELECT count(e) FROM ExaminrCoreBundle:Question e INNER JOIN e.module m WHERE e.deleted = FALSE AND e.type = '" . $type . "' " . ($module ? ("AND m.id = '" . $module . "'") : ""));

        return $qb->getSingleScalarResult();
    }

    public function getByModuleANDType($module, $type)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQuery("SELECT e.id FROM ExaminrCoreBundle:Question e INNER JOIN e.module m WHERE e.deleted = FALSE AND e.type = '" . $type . "' " . ($module ? ("AND m.id = '" . $module . "'") : ""));

        return $qb->getArrayResult();
    }
}