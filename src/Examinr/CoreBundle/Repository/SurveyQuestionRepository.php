<?php

namespace Examinr\CoreBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\Request;

class SurveyQuestionRepository extends EntityRepository
{
    public function filter(Request $request, $deleted = 'false')
    {
        $em = $this->getEntityManager();

        $start = is_numeric($request->query->get('start')) ? intval($request->query->get('start')) : 0;

        $lenght = is_numeric($request->query->get('length')) ? intval($request->query->get('length')) : 10;
//        if (!$start % $lenght != 0) {
//            $start = 0;
//        }

        $search = $request->query->get('search')['value'];

        $sortColumn = strtolower($request->query->get('order')[0]['column']);
        $sortColumn = $request->query->get('columns')[intval($sortColumn)]['name'];
        if ($sortColumn != 'text' && $sortColumn != 'topic') {
            $sortColumn = 'text';
        }

        $sortOrder = isset($request->query->get('order')[0]['dir']) ? $request->query->get('order')[0]['dir'] : 'asc';
        if ($sortOrder != 'asc' && $sortOrder != 'desc') {
            $sortOrder = 'asc';
        }

        $topic = $request->query->get('columns')[0]['search']['value'];
        $type = $request->query->get('columns')[2]['search']['value'];

//        var_dump($topic);
//        var_dump($type);

        $recordsFilteredCount = $em->createQuery("SELECT COUNT(e) FROM ExaminrCoreBundle:SurveyQuestion e INNER JOIN e.topic m WHERE e.text LIKE " . "'%" . $search . "%' " . (strlen($topic) != 0 ? "AND e.topic = '" . $topic . "'" : "") . " " . (strlen($type) != 0 ? "AND e.type = '" . $type . "'" : '') . " AND e.deleted = " . $deleted . " AND m.deleted = false")->getSingleScalarResult();
        if ($start == $recordsFilteredCount) {
            $start -= $start == 0 ? 0 : $lenght;
        }

        $recordsFilteredItems = $em->createQuery("SELECT e FROM ExaminrCoreBundle:SurveyQuestion e INNER JOIN e.topic m WHERE e.text LIKE " . "'%" . $search . "%' " . (strlen($topic) != 0 ? "AND e.topic = '" . $topic . "'" : "") . " " . (strlen($type) != 0 ? "AND e.type = '" . $type . "'" : '') . " AND e.deleted = " . $deleted . " AND m.deleted = false ORDER BY " . ($sortColumn == 'topic' ? "m.name" : ("e." . $sortColumn)) . " " . $sortOrder)
            ->setFirstResult($start)
            ->setMaxResults($lenght)
            ->getResult();

//        //CONCAT(d.estudiante_id, '') LIKE '%3%'

        return array(
            "recordsFiltered" => $recordsFilteredCount,
            "data" => $recordsFilteredItems
        );
    }

    public function count($deleted = 'false')
    {
        $em = $this->getEntityManager();

        $qb = $em->createQuery("SELECT count(e.id) FROM ExaminrCoreBundle:SurveyQuestion e WHERE e.deleted = " . $deleted);

        return $qb->getSingleScalarResult();
    }

    public function getOne($topic, $type, $index)
    {
        $em = $this->getEntityManager();

        $questions = $em->createQuery("SELECT e FROM ExaminrCoreBundle:SurveyQuestion e INNER JOIN e.topic m WHERE m.id = '" . $topic . "' AND e.type = '" . $type . "' ORDER BY e.id")
            ->setFirstResult($index)
            ->setMaxResults(1)
            ->getResult();

        if (sizeof($questions) > 0)
            return$questions[0];
        return null;
    }

    public function countByTopic($topic)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQuery("SELECT count(e) FROM ExaminrCoreBundle:SurveyQuestion e INNER JOIN e.topic m WHERE e.deleted = FALSE " . ($topic ? ("AND m.id = '" . $topic . "'") : ""));

        return $qb->getSingleScalarResult();
    }

    public function getByTopic($topic)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQuery("SELECT e.id FROM ExaminrCoreBundle:SurveyQuestion e INNER JOIN e.topic m WHERE e.deleted = FALSE " . ($topic ? ("AND m.id = '" . $topic . "'") : ""));

        return $qb->getArrayResult();
    }

    public function getFullObjectsByTopic($topic)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQuery("SELECT e FROM ExaminrCoreBundle:SurveyQuestion e INNER JOIN e.topic m WHERE e.deleted = FALSE " . ($topic ? ("AND m.id = '" . $topic . "'") : "") . " order by e.id");

        return $qb->getResult();
    }
}