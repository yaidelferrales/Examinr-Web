<?php

namespace Examinr\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\Common\Collections\ArrayCollection;
use JsonSerializable;


/**
 * ExamAnswerQuestion
 *
 * @ORM\Table(name="surveyanswer_question")
 * @ORM\Entity (repositoryClass="Examinr\CoreBundle\Repository\SurveyAnswerQuestionRepository")
 */
class SurveyAnswerQuestion
{
    /**
     * @ManyToOne(targetEntity="SurveyQuestion")
     * @JoinColumn(name="question_id", referencedColumnName="id")
     **/
    public $question;

    /**
     * @var \Examinr\CoreBundle\Entity\SurveyAnswer
     *
     * @ORM\ManyToOne(targetEntity="Examinr\CoreBundle\Entity\SurveyAnswer", inversedBy="questionAnswers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="surveyanswer_id", referencedColumnName="id")
     * })
     */
    public $surveyAnswer;

    /**
     * @var string
     *
     * @ORM\Column(name="answer", type="string", length=255, nullable=false)
     */
    public $answer = "";

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    public function __construct()
    {
        $this->questions = new ArrayCollection();
    }
}
