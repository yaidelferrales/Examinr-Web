<?php

namespace Examinr\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;
use Symfony\Component\Translation\Tests\String;

/**
 * Exam
 *
 * @ORM\Table(name="survey")
 * @ORM\Entity(repositoryClass="Examinr\CoreBundle\Repository\SurveyRepository")
 */
class Survey implements JsonSerializable
{
    /**
     * @var \Examinr\CoreBundle\Entity\SurveyTopic
     *
     * @ORM\ManyToOne(targetEntity="Examinr\CoreBundle\Entity\SurveyTopic")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="topic_id", referencedColumnName="id")
     * })
     */
    private $topic;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="teacher", type="string", length=255, nullable=false)
     */
    private $teacher;

    /**
     * @var string
     *
     * @ORM\Column(name="paralelo", type="string", length=255, nullable=false)
     */
    private $paralelo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="randomize_questions", type="boolean", nullable=false)
     */
    private $randomizeQuestions;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=false)
     */
    private $deleted = false;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * Set topic
     *
     * @param integer $topic
     * @return Survey
     */
    public function setTopic($topic)
    {
        $this->topic = $topic;

        return $this;
    }

    /**
     * Get topic
     *
     * @return SurveyTopic
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * Set teacher
     *
     * @param string $teacher
     * @return Survey
     */
    public function setTeacher($teacher)
    {
        $this->teacher = $teacher;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Survey
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get teacher
     *
     * @return string
     */
    public function getTeacher()
    {
        return $this->teacher;
    }

    /**
     * Set paralelo
     *
     * @param string $paralelo
     * @return Survey
     */
    public function setParalelo($paralelo)
    {
        $this->paralelo = $paralelo;

        return $this;
    }

    /**
     * Get paralelo
     *
     * @return string
     */
    public function getParalelo()
    {
        return $this->paralelo;
    }

    /**
     * Set randomizeQuestions
     *
     * @param boolean $randomizeQuestions
     * @return Exam
     */
    public function setRandomizeQuestions($randomizeQuestions)
    {
        $this->randomizeQuestions = $randomizeQuestions;

        return $this;
    }

    /**
     * Get randomizeQuestions
     *
     * @return boolean
     */
    public function getRandomizeQuestions()
    {
        return $this->randomizeQuestions;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Exam
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @return Exam
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * (PHP 5 &gt;= 5.4.0)<br/>
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     */
    public function jsonSerialize()
    {
        return array(
            "id" => $this->id,
            "teacher" => $this->teacher,
            "paralelo" => $this->paralelo,
            "topic" => $this->topic->getName(),
            "name" => $this->name,
            "active" => $this->active,
        );
    }
}
