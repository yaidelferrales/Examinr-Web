<?php

namespace Examinr\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;
use Symfony\Component\Translation\Tests\String;

/**
 * Exam
 *
 * @ORM\Table(name="exam")
 * @ORM\Entity(repositoryClass="Examinr\CoreBundle\Repository\ExamRepository")
 */
class Exam implements JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="base", type="integer", nullable=false)
     */
    private $base = 40;

    /**
     * @var \Examinr\CoreBundle\Entity\Module
     *
     * @ORM\ManyToOne(targetEntity="Examinr\CoreBundle\Entity\Module")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="module_id", referencedColumnName="id")
     * })
     */
    private $module;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="cant_questions_cc", type="integer", nullable=false)
     */
    private $cantQuestionsCc;

    /**
     * @var integer
     *
     * @ORM\Column(name="cant_questions_ci", type="integer", nullable=false)
     */
    private $cantQuestionsCi;

    /**
     * @var integer
     *
     * @ORM\Column(name="duration", type="integer", nullable=false)
     */
    private $duration;

    /**
     * @var boolean
     *
     * @ORM\Column(name="randomize_questions", type="boolean", nullable=false)
     */
    private $randomizeQuestions;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=false)
     */
    private $deleted = false;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * Set base
     *
     * @param integer $base
     * @return Exam
     */
    public function setBase($base)
    {
        $this->base = $base;

        return $this;
    }

    /**
     * Get base
     *
     * @return integer
     */
    public function getBase()
    {
        return $this->base;
    }

    /**
     * Set module
     *
     * @param integer $module
     * @return Exam
     */
    public function setModule($module)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module
     *
     * @return Module
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Exam
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set cantQuestionsCc
     *
     * @param integer $cantQuestionsCc
     * @return Exam
     */
    public function setCantQuestionsCc($cantQuestionsCc)
    {
        $this->cantQuestionsCc = $cantQuestionsCc;

        return $this;
    }

    /**
     * Get cantQuestionsCc
     *
     * @return integer
     */
    public function getCantQuestionsCc()
    {
        return $this->cantQuestionsCc;
    }

    /**
     * Set cantQuestionsCi
     *
     * @param integer $cantQuestionsCi
     * @return Exam
     */
    public function setCantQuestionsCi($cantQuestionsCi)
    {
        $this->cantQuestionsCi = $cantQuestionsCi;

        return $this;
    }

    /**
     * Get cantQuestionsCi
     *
     * @return integer
     */
    public function getCantQuestionsCi()
    {
        return $this->cantQuestionsCi;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     * @return Exam
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return integer
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Get formatedDuration
     *
     * @return String
     */
    public function getFormatedDuration()
    {
        $tmpduration = $this->duration;

        $result = $tmpduration % 60;
        if (strlen($result) == 1) {
            $result = '0' . $result;
        }
        $result = ':' . $result;
        $tmpduration = intval($tmpduration / 60);

        $result = ($tmpduration % 60) . $result;
        if (strlen($result) == 4) {
            $result = '0' . $result;
        }
        $result = ':' . $result;
        $tmpduration = intval($tmpduration / 60);

        $result = $tmpduration . $result;
        if (strlen($result) == 7) {
            $result = '0' . $result;
        }

        return $result;
    }

    /**
     * Set randomizeQuestions
     *
     * @param boolean $randomizeQuestions
     * @return Exam
     */
    public function setRandomizeQuestions($randomizeQuestions)
    {
        $this->randomizeQuestions = $randomizeQuestions;

        return $this;
    }

    /**
     * Get randomizeQuestions
     *
     * @return boolean
     */
    public function getRandomizeQuestions()
    {
        return $this->randomizeQuestions;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Exam
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @return Exam
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * (PHP 5 &gt;= 5.4.0)<br/>
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     */
    public function jsonSerialize()
    {
        return array(
            "id" => $this->id,
            "module" => $this->module->getName(),
            "name" => $this->getName(),
            "cantQuestionsCc" => $this->cantQuestionsCc,
            "cantQuestionsCi" => $this->cantQuestionsCi,
            "duration" => $this->getFormatedDuration(),
            "active" => $this->active,
        );
    }
}
